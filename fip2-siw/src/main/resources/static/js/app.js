var map;
var client;
var latitude;
var longitude;
var markers = [];
var infoWindows = [];

function initMap() {
	currentPosition(); // demander la geoposition
}

function currentPosition() { // recupere la geoposition
	if (typeof(navigator.geolocation) !== "undefined") {
		navigator.geolocation.getCurrentPosition(geopositionCallback);
	}
	// TODO Que faire quand navigator.geolocation est "undefined" ?
}

function geopositionCallback(geoposition) { // callback de navigator.geolocation.getCurrentPosition()
	latitude = geoposition.coords.latitude;
	longitude = geoposition.coords.longitude;

	// creer la map avec les coordonnees
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: latitude, lng: longitude},
		zoom: 14,
		scrollwheel: false
	});

	// ajouter le marqueur client avec les coordonnees
	client = new google.maps.Marker({
		map: map,
		position: new google.maps.LatLng({
			lat: latitude,
			lng: longitude
		}),
		title: "Votre position",
		icon: {
			url: "img/client-icon.png",
			anchor: new google.maps.Point(24, 52)
		}
	})

	// envoyer au serveur les coordonnees
	$.post("/poi", {latitude: latitude, longitude: longitude}, function(data) {
		// creer un marqueur pour chaque binding
		for (i in data.results.bindings) {
			markers[i] = new google.maps.Marker({
				map: map,
				position: new google.maps.LatLng({
					lat: parseFloat(data.results.bindings[i].lat.value),
					lng: parseFloat(data.results.bindings[i].lng.value)
				}),
				title: data.results.bindings[i].label.value,
				url: "//fr.wikipedia.org/wiki/" + data.results.bindings[i].label.value
			})

			// mettre un lien pour chaque marqueur
			google.maps.event.addListener(markers[i], 'click', function(innerKey) {
				return function() {
					OpenInNewTab(markers[innerKey].url);
				}
			}(i));
			
			// mettre une image pour chaque marqueur si thumbnail (OPTIONAL)
			if (typeof(data.results.bindings[i].thumbnail) !== "undefined") {
				infoWindows[i] = new google.maps.InfoWindow({
					content: thumbnailUrlToContentInfoWindow(data.results.bindings[i].thumbnail.value, markers[i].title)
				})
				
				google.maps.event.addListener(markers[i], 'mouseover', function(innerKey) {
					return function() {
						infoWindows[innerKey].open(map, markers[innerKey]);
					}
				}(i));
				
				google.maps.event.addListener(markers[i], 'mouseout', function(innerKey) {
					return function() {
						infoWindows[innerKey].close();
					}
				}(i));
			}
		}
	}, "json");
}

function thumbnailUrlToContentInfoWindow(imageUrl, label) {
	return "<img src=\"" + imageUrl + "\" alt=\"Image " + label + "\" style=\"max-height: 200px; max-width: 200px;\">";
}

function OpenInNewTab(url) {
	var win = window.open(url, '_blank');
	win.focus();
}