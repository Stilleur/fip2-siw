package com.mourgeon.web;

import java.io.UnsupportedEncodingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mourgeon.service.HomeService;

@Controller
public class HomeController {
	
	@Autowired
	private HomeService service;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getHome() {
		return "home";
	}
	
	@ResponseBody
	@RequestMapping(value = "/poi", method = RequestMethod.POST)
	public String postGeoposition(@RequestParam double latitude, @RequestParam double longitude) throws UnsupportedEncodingException {
		// Recupère les points d'intérêts 5km à la ronde
		return service.getPointsOfInterest(latitude, longitude, 5);
	}
}
