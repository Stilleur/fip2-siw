package com.mourgeon.service;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import org.springframework.stereotype.Service;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;

@Service
public class HomeService {

	// 1° latitude = 110,574 km
	private static final double ONE_KILOMETER_IN_LATITUDE_DEGREE = 1 / 110.574;
	private static final String SPARQL_ENDPOINT = "http://fr.dbpedia.org/sparql";

	public HomeService() {}

	/**
	 * Retourne les points d'intêrets proche sous le format JSON
	 * @param latitude La latitude
	 * @param longitude La longitude
	 * @param kilometersAround Effectue la recherche avec comme distance limite le nombre de kilomètres donnés
	 * @throws UnsupportedEncodingException 
	 */
	public String getPointsOfInterest(double latitude, double longitude, double kilometersAround) throws UnsupportedEncodingException {
		double dLatitude = ONE_KILOMETER_IN_LATITUDE_DEGREE * kilometersAround;
		double dLongitude = getOneKilometerInLongitudeDegree(latitude) * kilometersAround;

		String queryString = "   PREFIX rdf: <http://www.w3.org/2000/01/rdf-schema#>   "  + 
				"   PREFIX owl: <http://www.w3.org/2002/07/owl#>  "  + 
				"   PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>  "  + 
				"   PREFIX dbpediaowl: <http://dbpedia.org/ontology/>  "  + 
				"     "  + 
				"   SELECT DISTINCT ?label ?lat ?lng ?thumbnail  "  + 
				"   WHERE {  "  + 
				"   ?poi ?o owl:Thing .  "  + 
				"   ?poi rdf:label ?label . FILTER langMatches(lang(?label),'fr') .  "  + 
				"   ?poi geo:lat ?lat . filter(?lat >= " + (latitude - dLatitude) + ") . filter(?lat <= " + (latitude + dLatitude) + ") .       "  + 
				"   ?poi geo:long ?lng . filter(?lng >= " + (longitude - dLongitude) + ") . filter(?lng <= " + (longitude + dLongitude) + ") .  "  + 
				"   OPTIONAL {?poi dbpediaowl:thumbnail ?thumbnail}  "  + 
				"  }  " ; 

		Query query = QueryFactory.create(queryString);
		QueryExecution queryExecution = QueryExecutionFactory.sparqlService(SPARQL_ENDPOINT, query);
		ResultSet resultSet = queryExecution.execSelect();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ResultSetFormatter.outputAsJSON(outputStream, resultSet);
		String json = new String(outputStream.toByteArray());

		// iso to utf-8
		char[] chars = json.toCharArray();
		String out = new String(fix(chars), "utf-8");

		return out;
	}

	public static byte[] fix(char[] a) {
		byte[] b = new byte[a.length];
		for (int i = 0; i < a.length; i++) b[i] = (byte) a[i];
		return b;
	}

	private static double getOneKilometerInLongitudeDegree(double latitude) {
		// 1° longitude = 111.320*cos(latitude) km
		return 1 / (111.320 * Math.cos(latitude * Math.PI / 180)); // transformer les degrees en radians avant le cos (x * pi / 180)
	}
}
