package com.mourgeon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Fip2SiwApplication {

	public static void main(String[] args) {
		SpringApplication.run(Fip2SiwApplication.class, args);
	}
}
